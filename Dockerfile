# Build stage
FROM node:18 as build-stage
# Setting the working directory directory
WORKDIR /app
# Copying the package.json and package-lock.json to WORKDIR
COPY package*.json ./
# Copy the code to the WORKDIR
COPY . .
# Installing dependencies
RUN npm install
# Build the application
RUN npm run build

# Production stage
FROM nginx:1.21.3 as production-stage

# Copy configuration to docker container
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/dist /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
