# Common Query Engine and Viewer for PostNord

## Introduction:
Postnord AS wants us to explore solutions for extracting database information quick and
easily for the non-SQL user. Using developer time to extract reports from a database takes
developers away from developing and can become a costly bottle neck in daily operations.
In addition to developing a system for extracting database information, we look at different
API’s for communicating with the database and compare efficiency and ease of use.
This was accomplished by working with Scrum in an iterative approach as well as using
other know development strategies.
The product developed in this project is a prototype for extracting database information with
by using a web application with a simple user interface layout. In this way the non-SQL user
can interact with the site and extract information without any prior knowledge of SQL.

## Project Description:
Welcome to our prototype query search engine, developed by a group of bachelor students for PostNord. Our project aimed to empower non-SQL users by providing them with a user-friendly interface to make queries and retrieve information from a database. The objective was to reduce their reliance on developers and save valuable time.


Our solution allows users to find the information they seek without requiring any knowledge of SQL. By simplifying the query process, users can avoid the need to approach developers and ask for assistance, thereby enhancing their autonomy and productivity.


To get started with our query engine, simply navigate through the intuitive side menu or use the conveniently placed buttons below. With our application, you can effortlessly explore the database and retrieve the desired information with ease.

## Summary of Technologies:
* Backend: Spring Boot and GraphQL running in a Docker container in our server that are hosted at NTNU.
* Database: Informix running in a Docker container in our server that are hosted at NTNU.
* Frontend: Vue.js and Apollo running in a Docker container in our server that are hosted at NTNU.

## How to Use the Program:
* Start by clicking on the 'POSTNORD' logo positioned at the top center of the screen. This will take you to the home screen where you can begin your query journey.
* Explore the sub-menu options by clicking the arrows located in the top left corner. These options will allow you to navigate through different sections of the application, providing you with various query functionalities.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
