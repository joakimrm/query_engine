import {createApp, h, provide} from "vue";
import App from "./App.vue";
import router from "./router/index.js";
// Vuetify
import 'vuetify/styles'
import {createVuetify} from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import '@mdi/font/css/materialdesignicons.css'
// Apollo
import {DefaultApolloClient} from "@vue/apollo-composable";
import {
    ApolloClient,
    createHttpLink,
    InMemoryCache,
} from "@apollo/client/core";

// Change this to true if you want to use local database.
const developmentActive = false;
// Local database.
const localDatabase = "http://localhost:8080/graphql";
// Online database.
const OnlineDatabase = "http://129.241.153.20:8080/graphql";
// Link to database based on developmentActive.
const link = developmentActive ? localDatabase : OnlineDatabase;

const cache = new InMemoryCache();
const apolloClient = new ApolloClient({
    link: createHttpLink({
        uri: link,
    }),
    cache,
});

const vuetify = createVuetify({
    components,
    directives,
})

const app = createApp({
    setup() {
        provide(DefaultApolloClient, apolloClient);
    },
    render: () => h(App),
});
app.use(router);
app.use(vuetify);
app.mount("#app");
