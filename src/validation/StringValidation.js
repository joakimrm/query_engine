export function validateString(value) {
    return /^[a-zA-Z]+$/.test(value);
}
