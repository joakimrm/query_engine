import {createRouter, createWebHistory} from "vue-router";
import Home from "@/views/Home.vue";
import CustomersView from "@/views/CustomersView.vue";
import InvoicesView from '@/views/InvoicesView.vue';
import ParcelView from '@/views/ParcelView.vue';
import LocationView from '@/views/LocationView.vue';

function lazyLoad(view) {
    return () => import(`@/views/${view}.vue`)
}

const routes = [
    {
        path: '/:pathMatch(.*)*',
        component: Home,
        meta: {
            title: 'Home Page'
        }
    },
    {
        path: "/home",
        name: "Home",
        component: Home,
        meta: {
            title: 'Home Page'
        }
    },
    {
        path: "/customers",
        name: "CustomersView",
        component: lazyLoad('CustomersView'),
        meta: {
            title: 'Customer Information'
        }
    },
    {
        path: "/parcels",
        name: "ParcelView",
        component: lazyLoad('ParcelView'),
        meta: {
            title: 'Parcel Information'
        }
    },
    {
        path: "/locations",
        name: "LocationView",
        component: lazyLoad('LocationView'),
        meta: {
            title: 'Terminal Locations'
        }
    },
    {
        path: "/invoices",
        name: "InvoicesView",
        component: lazyLoad('InvoicesView'),
        meta: {
            title: 'Invoice Information'
        }
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;
